



function sanitize (obj){
    if (obj instanceof Object){
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (/^\$/.test(key)) {
                    delete obj[key];
                }else if (typeof obj[key] === "string"){
                    obj[key] = obj[key].trim();
                }else if (typeof obj[key] !== "number"){
                    sanitize(obj[key]);
                }
            }
        }
    }
}
function cleanBody (req, res, next) {
    "use strict";
    if (req.body instanceof Object) {
        for (var key in req.body) {
            if (req.body.hasOwnProperty(key)) {
                sanitize(req.body);
                if (key === '_id' || key === 'type' || key === 'dateCreated'){
                    delete req.params[key];
                }
            }
        }
    }
    next();
}
function cleanParams (req, res, next) {
    "use strict";
    if (req.params instanceof Object) {
        for (var key in req.params) {
            if (req.params.hasOwnProperty(key)) {
                sanitize(req.params[key]);

            }
        }
    }
    next();
};
function cleanAll (req, res, next) {
    "use strict";
    cleanParams(req,res,cleanBody(req,res,next));
};

module.exports.cleanBody = cleanBody;
module.exports.cleanParams = cleanParams;
module.exports.cleanAll = cleanAll;
var jwt = require('jsonwebtoken');
var serverConf = require('../config/server');
var Membre = require('mongoose').model('Membre');

module.exports = function(req,res,next){
    "use srict";
    var token = req.headers.authorization;
    if (!token){
        res.status(400);
        return res.json({success:false, msg:'Accès non autorisé.'});
    }
    jwt.verify(token, serverConf.secret, 
        function(err,payload){
            if (err)
                return res.status(500).json({success:false, msg:err});
            if (!payload)
                return res.status(503).json({success:false, msg:'Accès non autorisé.'});
        
            Membre.getById(payload.id, function(err, membre){
                if (err)
                    return res.status(500).json({success:false, msg:err});
                if (!membre)
                    return res.status(400).json({success:false, msg:'Accès non autorisé.'});
                req.membreId = parseInt(payload.id);
                next();
            });
        }
    );
};
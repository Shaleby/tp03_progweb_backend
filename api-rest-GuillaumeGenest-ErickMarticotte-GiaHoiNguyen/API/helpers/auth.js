var Membre = require('mongoose').model('Membre');
var Collection = require('mongoose').model('Collection');

function getAcestors(ancestorsId){
    "use strict";
    return new Promise(function(resolve, reject){
        var ancestors = [];
        for(var i =0; i< ancestorsId.length; i++){
            Membre.findOne({"children._id" : ancestorsId[i]}, {"children.$":1}, function (err, membre){
            if (err || !membre)
                reject();
                ancestors.push(membre.children[0]);
                if (ancestors.length === ancestorsId.length){
                    resolve(ancestors);
                }
            });
        }
    });
}
module.exports = function verifyAccess (memId, obj){
    "use strict";
    return new Promise(function(resolve, reject) {
        Membre.findOne({"children._id" : obj._id}, {"children.$":1}, function(err, membre){
            if (err || !membre)
                resolve(false);
            var ancestorsId = membre.children[0].ancestors;
            if (ancestorsId.length === 0)
                resolve(false);
            getAcestors(ancestorsId).then(function(ancestors){
                for(var i = 0; i< ancestors.length; i++){
                    if (ancestors[i].sharedWith.includes(memId))
                        resolve(true);
                }
                resolve(false);
            }, function(){resolve(false);});
        });
    });
};
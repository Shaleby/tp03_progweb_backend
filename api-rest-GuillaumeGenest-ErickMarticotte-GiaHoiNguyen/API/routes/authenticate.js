var express = require('express');
var router = express.Router();
var Collection = require('mongoose').model('Collection');
var Membre = require('mongoose').model('Membre');
var jwt = require('jsonwebtoken');
var serverConf = require('../config/server');

router.post('/', function(req,res,next){
    "use strict";
    var username = req.body.username;
    var password = req.body.password;
    
    Membre.getUserByUsername(username, function (err, user){
        if(err){
            res.status(500);
            return res.json({success:false, msg: err});
        }else if (!user){
            res.status(400);
            return res.json({success:false, msg: 'Utilisateur non trouvé.'});
        }else {
            Membre.comparePassword(password, user.password, function(err, isMatch){
                if(err){
                    res.status(500);
                    return res.json({success:false, msg: err});
                }else if (!isMatch){
                    res.status(400);
                    return res.json({success:false, msg:"Mauvais mot de passe ou nom d'utilisateur."});
                }else {
                    var payload = {id: user.id};
                    var token = jwt.sign(payload, serverConf.secret, {expiresIn: 604800});
                    res.status(200);
                    return res.json({success: true,msg:'La création du token a foncionnée.', data:token});
                }
            });

        }
    });
});

module.exports = router;
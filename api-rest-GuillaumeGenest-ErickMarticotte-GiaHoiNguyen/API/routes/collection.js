
var express = require('express');
var router = express.Router();
var Collection = require('mongoose').model('Collection');
//var Membre = require('mongoose').model('Membre');
var Counter = require('mongoose').model('Counter');
var serverConf = require('../config/server');
var sanitize = require('../helpers/sanitize');
var hasAuthorization = require('../helpers/auth');
var Collection = require('mongoose').model('Collection');
//var Lieu = require('mongoose').model('Lieu');0
/**
 * Permet de créer une ressource collection
 * avec ou sans collection mère
 * 
 *  {
 *      "nom": "nom de la catégorie (requis)",
 *      "idParent": "id du parent (optionel)"    
 *  }
 */
router.post('/', sanitize.cleanBody, function(req,res){
    "use strict";
    function create (){
        Collection.create(req.membreId, newCollection,function(err, collection){
            if (err){
                Counter.revertVal('collection');
                res.status(500);
                return res.json({success:false, msg:err});
            }else if(!collection){
                Counter.revertVal('collection');
                res.status(400);
                return res.json({success:false, msg: "Le collection n'a pas pu être créée."});
            }else{
                res.location(serverConf.api_url + '/Collection/' + collection._id);
                res.status(200);
                return res.json({
                    success: true,
                    msg: 'La collection a bien été créée',
                    data: collection
                });
            }
        });
    }
    var newCollection = new Collection(req.body);
    Counter.getNextVal('collection', function (err, seq){
        if (!err){
            newCollection._id = seq;
            if (req.body.parent) {
                newCollection.parent = req.body.parent;
                Collection.findById(newCollection.parent, function(err, membre){
                    if (err){
                        Counter.revertVal('collection');
                        res.status(500);
                        return res.json({success:false, msg:err});
                    }else if(!collection || collection.length < 0){
                        Counter.revertVal('collection');
                        res.status(400);
                        return res.json({success:false, msg: "La collection parent n'existe pas."});
                    }else{
                        if (membre._id !== req.membreId){
                            Counter.revertVal('collection');
                            res.status(403);
                            return res.json({success: false, msg:"Vous n'avez pas accès à la collection parent."});
                        }else {
                            Collection.findSiblings(newCollection.parent, {"categorie": newCollection.categorie}, function(err,doc){
                                if(err){
                                    Counter.revertVal('collection');
                                    res.status(500);
                                    return res.send({success:false, msg:err});
                                }else if (doc){
                                    Counter.revertVal('collection');
                                    res.status(400);
                                    return res.send({success:false, msg: "Il y a déjà une collection portant le même nom."});
                                }else {
                                    Collection.addObjToCollection(newCollection.parent, newCollection._id, function(err, membre){
                                        if (err){
                                            Counter.revertVal('collection');
                                            res.status(500);
                                            return res.json({success:false, msg:err});
                                        }else if(!doc || doc.length < 0){
                                            Counter.revertVal('collection');
                                            res.status(400);
                                            return res.json({success:false, msg: "La collection n'a pas pu être ajouté au parent."});
                                        }else {
                                            var parent = membre.children.filter(function(c){
                                                return c._id === newCollection.parent;
                                            })[0];
                                            newCollection.ancestors = parent.ancestors;
                                            newCollection.ancestors.push(newCollection.parent);        
                                            create();
                                        }
                                    }); //end of ad objToCollection
                                } // End of if
                            });  // End of find siblings
                        } // End of if
                    }
                });// End of find by id
            }else {
                Collection.findSiblings(newCollection.parent, {"categorie": newCollection.categorie}, function(err,doc){
                    if(err){
                        Counter.revertVal('collection');
                        res.status(500);
                        return res.send({success:false, msg:err});
                    }else if (doc){
                        Counter.revertVal('collection');
                        res.status(400);
                        return res.send({success:false, msg: "Il y a déjà une collection portant le même nom."});
                    }else {
                        create();
                    } // End of if
                });  // End of find siblings
            }
        }else {
            res.status(500);
            return res.json({success:false, msg:"La collection n'a pas pu être créée."});
        }
    });
});

/**
 * Consulter toutes les collections d'un membre
 * Faire une recherche
 */
router.get('/', function(req,res){
    "use strict";
    if (req.query.search){
        var result = [];
        Collection.search(req.query.search, function(err, membre){
            if (err){
                res.status(500);
                return res.json({success:false, msg:err});
            }else if (membre.length === 0){
                res.status(400);
                return res.json({success:true, msg:"Aucune collection ne correspond à la recherche", data:[]});
            }
            for (var i = 0; i<membre.length; i++){
                if (membre[i]._id === req.membreId){
                    result.push(membre[i].children);
                }else {
                    for (var j = 0; j<membre[i].children.length; j++ ){
                        if (hasAuthorization(req.membreId, membre[i].children[j] )){
                            result.push(membre[i].children[j]);
                        }
                    }
                }
            }
            return res.json({success:true, msg:"", data:result});
        });
    }else {
        Collection.findAll(req.membreId, function(err,membre){
            if (err)
                return res.json({success:false, msg:err});
            if (!membre)
                return res.json({success:true, msg:"Le membre n'a aucune collection", data:[]});
            if (membre._id !== req.membreId){
                for (var i = 0; i<membre.length; i++){
                    if (membre[i]._id === req.membreId){
                        result.push(membre[i].children);
                    }else {
                        for (var j = 0; j<membre[i].children.length; j++ ){
                            if (hasAuthorization(req.membreId, membre[i].children[j] )){
                                result.push(membre[i].children[j]);
                            }
                        }
                    }
                }
            }
            return res.json({success:true, msg:"Les collections on été trouvés", data:result});
        });
    }

});

/**
 * Consulter une collection
 * @param: id = id de la collection à consulter
 */
router.get('/:id', function(req,res){
    "use strict";
    Collection.findById(parseInt(req.params.id), function(err, membre){
        if (err){
            res.status(500);
            return res.json({success:false, msg:err});
        }else if (!membre){
            res.status(404);
            return res.json({success:false, msg:"La collection n'a pas pu être trouvé", data:[]});
        }else if (membre._id !== parseInt(req.membreId)){
            hasAuthorization(req.membreId, membre.children[0]).then(function(result){
                if (result){
                    res.status(200);
                    return res.json({success:true, msg:err, data:membre.children});
                }else {
                    res.status(403);
                    return res.json({success:false, msg:"Le membre n'a pas les accès nécessaires"});
                }
            });
        }else {
            res.status(200);
            return res.json({success:false, msg:"La collection a été trouvé.", data:membre.children[0]});
        }
    });
});
/**
 * Modifier une collection
 * @param: id = id de la collection à modifier
 */
router.put('/:id', function(req,res){
    "use strict";
    var nouvelleCollection = {};
    function update(){
        Collection.update(parseInt(req.params.id),nouvelleCollection, function(err,membre){
            if(err){
                res.status(500);
                return res.json({success:false, msg:err});
            }else if(!membre){
                res.status(404);
                return res.json({success:false, msg:"La collection n'a pas été modifiée."});                
            }
            res.status(200);         
            return res.json({success:true, msg:"La collection a été modifié", data:membre.children[0]});
        });
    }
    Collection.findById(parseInt(req.params.id), function(err, membre){
        if (err){
            res.status(500);
            return res.json({success:false, msg:err});
        }else if (!membre){
            res.status(404);
            return res.json({success:false, msg:"La collection n'a pas pu être trouvé", data:[]});
        }else if (membre._id !== req.membreId){
            res.status(403);
            return res.json({success:false, msg:"Le membre n'a pas les accès nécessaires"});
        }else {
            var vielleCollection = membre.children[0];
            
            var keys = Object.keys(vielleCollection);
            for (var i = 0; i<keys.length; i++){
                if (req.body[keys[i]] !== null && req.body[keys[i]] !== vielleCollection[keys[i]]){
                    nouvelleCollection["children.$." + keys[i]] = req.body[keys[i]];
                }
            }
            //var error = false;

            // Si ajout parent
            if (req.body.parent){
                Collection.findById(nouvelleCollection.parent, function(err,membre){
                    if (err){
                        res.status(500);
                        return res.json({success:false, msg:err});
                    }else if (!membre){
                        res.status(404);
                        return res.json({success:false, msg:"Le parent n'a pas pu être trouvé", data:[]});
                    }else if (membre._id !== req.membreId){
                        res.status(403);
                        return res.json({success:false, msg:"Le membre n'a pas les accès nécessaires"});
                    }else {
                        // Ajoute le lieu
                        Collection.addObjToCollection(nouvelleCollection, function(err, membre){
                            if (err){
                                res.status(500);
                                res.json({success:false, msg:err});
                            }else if (!membre){
                                res.status(404);
                                res.json({success:false, msg:"La collection parent n'a pas été trouvé."});
                                update();
                            }
                        });
                    }
                });
            }else {
                update();
            }
        }
    });
});
/**
 * Supprimer une collection
 * @param: id = id de la collection à supprimer
 */
router.delete('/:id', function(req,res){
    "use strict";
    Collection.findById(parseInt(req.params.id), function(err, membre){
        if (err){
            res.status(500);
            return res.json({success:false, msg:err});
        }else if (!membre){
            res.status(404);
            return res.json({success:false, msg:"La collection n'a pas pu être trouvé", data:[]});
        }else if (membre._id !== parseInt(req.membreId)){
            res.status(403);
            return res.json({success:false, msg:"Le membre n'a pas les accès nécessaires"});
        }else {
            var collectionASupprimer = membre.children[0];
            Collection.deleteById(collectionASupprimer._id, function(err, membre){
                if (err){
                    res.status(500);
                    return res.json({success:false, msg:"500 2e if", collectionASupprimer});
                }else if (!membre){
                    res.status(404);
                    return res.json({success:false, msg:"La collection n'a pas pu être supprimée 1.", data:[]});
                }else {
                    if (collectionASupprimer.parent !== ''){
                        Collection.findById(collectionASupprimer.parent, function(err, membre){
                            if (err){
                                res.status(500);
                                return res.json({success:false, msg:"500 3e if"});
                            }else if (!membre){
                                res.status(404);
                                return res.json({success:false, msg:"La collection parent n'a pas pu être trouvée", data:[]});
                            }else {
                                Collection.removeObjFromCollection(membre.children[0]._id, collectionASupprimer._id, function(err, membre){
                                    if (err){
                                        res.status(500);
                                        return res.json({success:false, msg:"500 4e if"});
                                    }else if (!membre){
                                        res.status(404);
                                        return res.json({success:false, msg:"La collection n'a pas pu être supprimée 2", data:[]});
                                    }else {
                                        res.status(204);
                                        return res.json({success:false, msg:"La collection a bien été supprimée."});
                                    }
                                });
                            }
                        });
                    }else {
                        res.status(204);
                        return res.json({success:false, msg:"La collection a bien été supprimée."});
                    }
                }
            });
        }
    });
    
});

module.exports = router;
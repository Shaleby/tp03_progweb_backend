var express = require('express');
var router = express.Router();
var Lieu = require('mongoose').model('Lieu');
var Geometry = require('mongoose').model('Geometry');
var hasAuthorization = require('../helpers/auth');
var Counter = require('mongoose').model('Counter');
var mongoose = require('mongoose');
var Membre = mongoose.model('Membre');
var sanitize = require('../helpers/sanitize');
var serverConf = require('../config/server');
var Collection = require('mongoose').model('Collection');

router.post('/', sanitize.cleanBody, function(req,res){
    "use strict";

    function create (){
        Lieu.create(req.membreId, newLieu,function(err, lieu){
            if (err){
                Counter.revertVal('collection');
                res.status(500);
                return res.json({success:false, msg:err});
            }else if(!lieu){
                Counter.revertVal('collection');
                res.status(400);
                return res.json({success:false, msg: "Le lieu n'a pas pu être créé."});
            }else{
                res.location(serverConf.api_url + '/Lieux/' + lieu._id);
                res.status(201);
                return res.json({
                    success: true,
                    msg: 'Le lieu a bien été créée.',
                    data: lieu
                });
            }
        });
    }
    var newLieu = new Lieu(req.body);
    Counter.getNextVal('collection', function (err, seq){
        if (!err){
            newLieu._id = seq;
            if (req.body.parent) {
                newLieu.parent = req.body.parent;
                Collection.findById(newLieu.parent, function(err, membre){
                    if (err){
                        Counter.revertVal('collection');
                        res.status(500);
                        return res.json({success:false, msg:err});
                    }else if(!membre){
                        Counter.revertVal('collection');
                        res.status(400);
                        return res.json({success:false, msg: "La collection parent n'existe pas."});
                    }else{
                        if (membre._id !== req.membreId){
                            Counter.revertVal('collection');
                            res.status(403);
                            return res.json({success: false, msg:"Vous n'avez pas accès à la collection parent."});
                        }else {
                            Lieu.findSiblings(newLieu.parent, {"name": newLieu.name}, function(err,doc){
                                if(err){
                                    Counter.revertVal('collection');
                                    res.status(500);
                                    return res.send({success:false, msg:err});
                                }else if (doc){
                                    Counter.revertVal('collection');
                                    res.status(400);
                                    return res.send({success:false, msg: "Il y a déjà un lieu portant le même nom."});
                                }else {
                                    Collection.addObjToCollection(newLieu.parent, newLieu._id, function(err, membre){
                                        if (err){
                                            Counter.revertVal('collection');
                                            res.status(500);
                                            return res.json({success:false, msg:err});
                                        }else if(!doc || doc.length < 0){
                                            Counter.revertVal('collection');
                                            res.status(400);
                                            return res.json({success:false, msg: "Le lieu n'a pas pu être ajouté au parent."});
                                        }else {
                                            var parent = membre.children.filter(function(c){
                                                return c._id === newLieu.parent;
                                            })[0];
                                            newLieu.ancestors = parent.ancestors;
                                            newLieu.ancestors.push(newLieu.parent);        
                                            create();
                                        }
                                    }); //end of ad objToCollection
                                } // End of if
                            });  // End of find siblings
                        } // End of if
                    }
                });// End of find by id
            }else {
                Lieu.findSiblings(newLieu.parent, {"name": newLieu.name}, function(err,doc){
                    if(err){
                        Counter.revertVal('collection');
                        res.status(500);
                        return res.send({success:false, msg:err});
                    }else if (doc){
                        Counter.revertVal('collection');
                        res.status(400);
                        return res.send({success:false, msg: "Il y a déjà un lieu portant le même nom."});
                    }else {
                        create();
                    } // End of if
                });  // End of find siblings
            }
        } else {
            res.status(500);
            return res.json({success:false, msg:"Le lieu n'a pas pu être créé."});
        }
    });
});
/**
 * Permet de consulter tous les lieux d'un membre
 * et de faire une recherche
 */
router.get('/', function(req,res){
    "use strict";
    if (req.query.search){
        var result = [];
        Lieu.search(req.query.search, function(err, membre){
            if (err){
                res.status(500);
                return res.json({success:false, msg:err});
            }else if (membre.length === 0){
                res.status(400);
                return res.json({success:true, msg:"Aucun lieu ne correspond à la recherche", data:[]});
            }
            for (var i = 0; i<membre.length; i++){
                if (membre[i]._id === req.membreId){
                    result.push(membre[i].children);
                }else {
                    for (var j = 0; j<membre[i].children.length; j++ ){
                        if (hasAuthorization(req.membreId, membre[i].children[j] )){
                            result.push(membre[i].children[j]);
                        }
                    }
                }
            }
            return res.json({success:true, msg:"", data:result});
        });
    }else {
        Lieu.findAll(req.membreId, function(err,membre){
            if (err)
                return res.json({success:false, msg:err});
            if (!membre)
                return res.json({success:true, msg:"Le membre n'a aucun lieu", data:[]});
            if (membre._id !== req.membreId){
                for (var i = 0; i<membre.length; i++){
                    if (membre[i]._id === req.membreId){
                        result.push(membre[i].children);
                    }else {
                        for (var j = 0; j<membre[i].children.length; j++ ){
                            if (hasAuthorization(req.membreId, membre[i].children[j] )){
                                result.push(membre[i].children[j]);
                            }
                        }
                    }
                }
            }
            return res.json({success:true, msg:err, data:result});
        });
    }
});
/**
 * Permet de consulter un lieu avec un id
 */
router.get('/:id', function(req,res){
    "use strict";
    Lieu.findById(parseInt(req.params.id), function(err, membre){
        if (err){
            res.status(500);
            return res.json({success:false, msg:err});
        }else if (!membre){
            res.status(404);
            return res.json({success:false, msg:"Le lieu n'a pas pu être trouvé", data:[]});
        }else if (membre._id !== parseInt(req.membreId)){
            hasAuthorization(req.membreId, membre.children[0]).then(function(result){
                if (result){
                    res.status(200);
                    return res.json({success:true, msg:err, data:membre.children});
                }else {
                    res.status(403);
                    return res.json({success:false, msg:"Le membre n'a pas les accès nécessaires"});
                }
            });
        }else {
            res.status(200);
            return res.json({success:true, msg:"Le lieu a été trouvé.", data:membre.children[0]});
        }
    });
});
/**
 * Permet de modifier un lieu 
 * @param : nom(optionel), desciprtion(optionel), geometry(optionel), idParent(optionel)
 */
router.put('/:id', function(req,res){
    "use strict";
    var nouveauLieu = {};
    function update(){
        Lieu.update(parseInt(req.params.id), nouveauLieu, function(err, membre){
            if (err){
                res.status(500);
                return res.json({success:false, msg:err});
            }else if (!membre){
                res.status(400);
                return res.json({success:false, msg:"Le lieu n'a pas pu être modifié."});
            }
            res.status(200);
            return res.json({success:true, msg:"Le lieu a bien été modifié.", data:nouveauLieu});
        });   
    }
    // Si le lieu existe
    Lieu.findById(parseInt(req.params.id), function(err, membre){
        if (err){
            res.status(500);
            return res.json({success:false, msg:err});
        }else if (!membre){
            res.status(400);
            return res.json({success:false, msg:"Le lieu n'a pas pu être trouvé", data:[]});
        }else if (membre._id !== req.membreId){
            res.status(403);
            return res.json({success:false, msg:"Le membre n'a pas les accès nécessaires"});
        }else {
            var vieuxLieu = membre.children[0];
            
            var keys = Object.keys(vieuxLieu);
            Object.assign(nouveauLieu, vieuxLieu);
            for (var i = 0; i<keys.length; i++){
                if (req.body[keys[i]] !== null && req.body[keys[i]] !== vieuxLieu[keys[i]]){
                    nouveauLieu[keys[i]] = req.body[keys[i]];
                }
            }
            //var error = false;
            // Si ajout parent
            if (req.body.parent){
                nouveauLieu.parent = parseInt(nouveauLieu.parent);
                Collection.findById(parseInt(nouveauLieu.parent), function(err,membre){
                    if (err){
                        res.status(500);
                        return res.json({success:false, msg:err});
                    }else if (!membre){
                        res.status(400);
                        return res.json({success:false, msg:"Le parent n'a pas pu être trouvé", data:[]});
                    }else if (membre._id !== req.membreId){
                        res.status(403);
                        return res.json({success:false, msg:"Le membre n'a pas les accès nécessaires"});
                    }else {
                        // Ajoute le lieu
                        nouveauLieu.ancestors = membre.children[0].ancestors;
                        nouveauLieu.ancestors.push(membre.children[0]._id);
                        Collection.addObjToCollection(nouveauLieu.parent, nouveauLieu._id, function(err, membre){
                            if (err){
                                res.status(500);
                                res.json({success:false, msg:err});
                            }else if (!membre){
                                res.status(400);
                                res.json({success:false, msg:"Le lieu n'a pas pu être ajouté à la collection parent."});

                            }else {
                                Collection.removeObjFromCollection(nouveauLieu.parent, nouveauLieu._id, function(err, membre){
                                    if (err){
                                        res.status(500);
                                        res.json({success:false, msg:err});
                                    }else if (!membre){
                                        res.status(400);
                                        res.json({success:false, msg:"Le lieu n'a pas pu être enlevé de la vielle collection parent."});
                                    }else {
                                        update();
                                    }
                                });
                            }
                        });
                    }
                });
            }else {
                update();
            }
        }
    });

    // si l'id du parent est spécifié, déplacer dans le parent puis modifier
});
router.delete('/:id', sanitize.cleanBody, function(req,res){
    "use strict";
    //Si le lieu n'existe pas
    if(req._id===null){
        res.status(404);
        return res.json({success:false, msg:"Le lieu n'existe pas."});    
    } else {
        Lieu.findById(req.params.id,function(err,membre){
            if (err){
                res.status(500);
                return res.json({success:false, msg:err});      
            }else if (!membre){
                res.status(404);
                return res.json({success:false, msg:err});
            }else if (membre._id!== req.membreId){
                res.status(403);
                return res.json({success:false,msg:"Le lieu n'appartient pas à l'utilisateur connecté."});
            }else {
                Collection.removeObjFromCollection(membre.children[0].parent,membre.children[0]._id, function(err,membre){
                    //Si erreur
                    if(err){
                        res.status(500);
                        return res.json({success:false, msg:err});      
                    }else if(req.parent !== Collection._id){
                        res.status(403);
                        return res.json({success:false, msg:"Le lieu n'existe pas dans la collection."});    
                        //Ok
                    }else{
                        res.status(201);
                        return res.json({
                            success: true,
                            msg: 'Le lieu a bien été supprimé.',
                            }
                        );       
                    }
                });
            }
            
        });
        
    } 

});

module.exports = router;


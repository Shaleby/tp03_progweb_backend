var express = require('express');
var router = express.Router();
var Membre = require('mongoose').model('Membre');
var jwt = require('jsonwebtoken');
var serverConf = require('../config/server');
var Counter = require('mongoose').model('Counter');

router.post('/', function(req,res,next){
    "use strict";
        var newUser = new Membre({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        });
    
            Membre.getUserByUsername(req.body.username, function(err, user){
                    if(err){
                        res.status(500);
                        res.json({success:false, msg: "Échec de l'enregistrement getByUsername"});
                    }else if(user){ 
                        res.status(400);
                        res.json({success:false, msg:"Nom d'utilisateur déjà utilisé"});
                    }else if(!user){
                    
                        if (req.body.username === ""){
                            res.status(403);
                            res.json({success:false, msg:"Nom d'utilisateur invalide"});
                        }else if (req.body.email === ""){
                            res.status(403);
                            res.json({success:false, msg:"Email invalide"});
                        }else if (req.body.password === ""){
                            res.status(403);
                            res.json({success:false, msg:"Mot de passe invalide"});
                        }else{
                            Membre.create(newUser, function(err, user){   
                                if(err){
                                    res.status(500);
                                    res.json({success:false, msg: "Échec de l'enregistrement create"});
                                }else
                            res.status(201);
                            res.json({success:true, msg: 'Utilisateur enregistré', user});
                        });
                    }
            };
       });}
);
module.exports = router;
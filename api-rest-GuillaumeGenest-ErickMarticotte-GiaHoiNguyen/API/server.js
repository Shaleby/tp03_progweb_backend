// Module
var express = require('express');
var serverConfig = require('./config/server');
var dbConfig = require('./config/db');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var port = process.env.PORT || serverConfig.port;
var sslRedirect = require('heroku-ssl-redirect');

// MongoDb
var connection = mongoose.connect(dbConfig.connectionStr);
mongoose.connection.on('connected', function(){
    "use strict";
    console.log('Connecté à la base de donnée: ' + dbConfig.connectionStr);
});
mongoose.connection.on('error', function (err){
    "use strict";
    console.log('Erreur de base de donnée: ' + err);
});

// AutoIncrement (for id)
autoIncrement.initialize(connection);

// App
var app = express();


// Models
require('./models/membre');
require('./models/lieu');
require('./models/collection');

// Counters
var Counter = require('./models/helpers/counter');
Counter.instanciate('collection');
Counter.instanciate('lieu');

// Routers
var user = require('./routes/membre');
var authenticate = require('./routes/authenticate');
var lieu = require('./routes/lieu');
var collection = require('./routes/collection');


//force https redirect
app.use(sslRedirect());
// Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Sécurité
var auth = require('./helpers/verifyToken');

// Routes
app.use(express.static('public'));
app.get('/', function(req,res){
    res.json({success:true, msg:"L'API est fonctionnel."});
});
app.use('/membres', user);
app.use('/authenticate', authenticate);
app.use('/collections',auth,collection);
app.use('/lieux',auth,lieu);


app.listen(port, function(){
    "use strict";
    console.log('Server started on port ' + port);
});
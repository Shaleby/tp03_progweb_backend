var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var Membre = require('mongoose').model('Membre');
var serverConf = require('../config/server');

module.exports = function (passport) {
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('Bearer');
    opts.secretOrKey = serverConf.secret;
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        console.log(jwt_payload);
      Membre.getById(jwt_payload._doc._id, (err, user) => {
        if(err){
          return done(err, false);
        }
        if(user){
          return done(null, user);
        } else {
          return done(null, false);
        }
      });
    }));
};

// function (jwt_payload, done) {
//     Membre.getById(jwt_payload.id, function (err, user) {
//         if (err) {
//             return done(err, false);
//         }
//         if (user) {
//             return done(null, user);
//         } else {
//             return done(null, false);
//         }
//     });
// }


// function(jwt_payload){
//     Membre.getById(jwt_payload.id, function(err, user){
//         if (err)
//             return 
//     });
// })
// );
var mongoose = require('mongoose');
var Counter = require('./helpers/counter');
var Membre = mongoose.model('Membre');
var Lieu = require('mongoose').model('Lieu');


/**
 * Model pour les collections de place
 */
var collectionSchema = mongoose.Schema({
    _id: Number,
    categorie: {
        type: String,
        required: true,
        index:true
    },
    type: {
        type: String,
        required: true,
        default: "collection"
    },
    children: {
        type: [Number],
        required: false,
        index: true
    },
    parent: {
        type:Number,
        required: false
    },
    ancestors: {
        type:[Number],
        required:false
    },
    sharedWith:{
        type: [Number],
        required: false
    }
});

var Collection = 
    module.exports = 
    mongoose.model('Collection', collectionSchema);
module.exports.type = 'collection';

module.exports.addObjToCollection = function (parentId, objId, callback) {
    "use strict";
    Membre.findOneAndUpdate({"children._id": parentId},{ "$push": {"children.$.children": objId}}, {new:true, "children.$":1}, callback);
};
module.exports.removeObjFromCollection = function (parentId, objId, callback) {
    "use strict";
    Membre.findOneAndUpdate({"children._id": parentId},{ "$pull": {"children.$.children": objId}}, {new:true, "children.$":1}, callback);
};
module.exports.findById = function (id, callback){
    "use strict";
    Membre.findOne({"children":{ "$elemMatch": { "_id": id, "type": Collection.type  } }},
    {"children.$": 1}, callback);
}
module.exports.findAll = function(membreId, callback){
    "use strict";
    Membre.findOne({"_id": membreId, "children.type": Collection.type}, callback);
};
module.exports.update = function(id, params, callback){
    "use strict";
    Membre.findOneAndUpdate({"children._id":id, 'children.type': Lieu.type}, {$set: params},{new: true,"children.$":1 }, callback);
};
/**
 * Permet d'ajouter une collection de places dans la bd
 */
module.exports.create = function (membreId, collection, callback) {
    "use strict";
    Membre.getById(membreId, function(err,membre){
        if (err || !membre)
            callback(err,membre);
        membre.children.push(collection);
        membre.save(function(err, membre){
            if (err || !membre)
                callback(err, membre);
            callback(err,collection);
        });
    });
};
module.exports.search = function (query, callback){
    "use strict";
    query = new RegExp(query);
    Membre.find({"children": 
        {
            $elemMatch:{
                "type":Collection.type, 
                $or: [
                    { "categorie": {$regex:query, $options:'i'}}
                ]
            }
        }
    }, callback);
};
/**
 * Permet de trouver les relation frères d'une collection
 * @param  {Integer} membreId
 * @param  {String} id
 * @param  {} params
 * @param  {} callback
 */
module.exports.findSiblings = function(parentId,params, callback){
    "use strict";
    var children = {
        "type": Collection.type,
        "parent":parentId
    };
    var keys = Object.keys(params);
    for (var i = 0; i< keys.length; i++){
        children[keys[i]] = params[keys[i]];
    }
    Membre.findOne({"children":{ "$elemMatch": children}},{"children.$":1}, callback);
};

/**
 * Permet de trouver les relation frères d'une collection
 * @param  {Integer} membreId
 * @param  {String} id
 * @param  {} params
 * @param  {} callback
 */
module.exports.deleteById = function(idParent, callback){
    "use strict";
    getAllChildren(idParent).then(function(arrayASupprimer){
        if (arrayASupprimer){
            arrayASupprimer.push(idParent);
            Membre.findOneAndUpdate({},{ "$pull": {"children":  {"_id":{$in :arrayASupprimer}}}}, callback);
        }else {
            callback("n'a pas pu trouver les enfants", null);
        }
    });
    
    
};
function getAllChildren(idParent){
    return new Promise(function (resolve, reject){
        Membre.aggregate([
            {
               "$project": {
                   "children": 1
               }
            },
            { "$match" : { "children" : { "$elemMatch" : { "ancestors" : idParent}}}},
            {
               "$unwind": "$children"
            }
        ], function (err, membres){
            if (err || membres.length === 0){
                resolve(false);
            }else {
                var idsEnfants = membres.map(function(v){return v.children._id;})
                resolve(idsEnfants);
            }
        });
    });
}
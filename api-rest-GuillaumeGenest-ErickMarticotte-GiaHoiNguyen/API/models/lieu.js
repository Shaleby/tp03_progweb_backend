var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var geometryType = ["Point", "MultiPoint", "LineString", "Polygon"];
var Counter = require('./helpers/counter');
var Membre = mongoose.model('Membre');

/**
 * Model pour les coordonnées
 */
var geometrySchema = mongoose.Schema({
    type: {
        type: String,
        required: true,
        enum: geometryType
    },
    coordinates: {
        type: [],
        required: true,
    }
});
geometrySchema.plugin(autoIncrement.plugin, 'Geometry');
var Geometry = 
    module.exports.Geometry = 
    mongoose.model('Geometry', geometrySchema);

/**
 * Model pour les place
 */
var lieuSchema = mongoose.Schema({
    _id: Number,
    name: {
        type: String,
        required: true,
        index: true
    },
    description: {
        type: String,
        required: false,
        index: true
    },
    dateCreated: {
        type: Date,
        required: false,
        default: Date.now
    },
    geometry: {
        type: geometrySchema,
        required: true
    },
    parent: {
        type:Number,
        required: false,
        index:true
    },
    type: {
        type:String,
        required: true,
        default: "lieu"
    },
    sharedWith:{
        type: [Number],
        required: false
    },
    ancestors: {
        type:[Number],
        required:false
    },
    comment:{
        type: String,
        required: false
    }
});
// lieuSchema.index({"name": 1, "description": 1});
var Lieu = 
    module.exports = 
    mongoose.model('Lieu', lieuSchema);
module.exports.type = 'lieu';
module.exports.search = function (query, callback){
    "use strict";
    query = new RegExp(query);
    Membre.find({"children": 
        {
            $elemMatch:{
                "type":Lieu.type, 
                $or: [
                    { "name": {$regex:query, $options:'i'}}, 
                    { "description":  {$regex:query, $options:'i'}}
                ]
            }
        }
    }, callback);
};
module.exports.findAll = function(membreId, callback){
    "use strict";
    Membre.findOne({"_id": membreId, "children.type": Lieu.type}, callback);
};
module.exports.findById = function(id, callback){
    Membre.findOne({"children":{ "$elemMatch": { "_id": id, "type": Lieu.type  } }},
    {"children.$": 1}, callback);
};
module.exports.update = function(id, params, callback){
    Membre.findOneAndUpdate({"children":{ "$elemMatch": { "_id": id, "type": Lieu.type  } }}, {$set: {"children.$": params}},{new: true,"children.$":1 }, callback);
};

/**
 * Permet de trouver les relation frères d'une collection
 * @param  {Integer} membreId
 * @param  {String} id
 * @param  {} params
 * @param  {} callback
 */
module.exports.findSiblings = function(parentId,params, callback){
    "use strict";
    var children = {
        "type": Lieu.type,
        "parent":parentId
    };
    var keys = Object.keys(params);
    for (var i = 0; i< keys.length; i++){
        children[keys[i]] = params[keys[i]];
    }
    Membre.findOne({"children":{ "$elemMatch": children}},{"children.$":1}, callback);
};
/**
 * Permet d'ajouter une collection de places dans la bd
 */
module.exports.create = function (membreId, collection, callback) {
    "use strict";
    Membre.getById(membreId, function(err,membre){
        if (err || !membre)
            callback(err,membre);
        membre.children.push(collection);
        membre.save(function(err, membre){
            if (err || !membre)
                callback(err, membre);
            callback(err,collection);
        });
    });
};

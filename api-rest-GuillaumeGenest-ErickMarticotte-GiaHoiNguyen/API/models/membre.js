var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var autoIncrement = require('mongoose-auto-increment');
var serverConf = require('../config/server');


var userSchema = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    children:{
        type: [mongoose.Schema.Types.Mixed],
        required: false
    }
});
userSchema.index({ "children.name": "text", "children.description":"text" });
userSchema.index({ "children.children": 1});
userSchema.index({ "children.parent": 1});

userSchema.plugin(autoIncrement.plugin, 'Membre');
var Membre = module.exports = mongoose.model('Membre', userSchema);
module.exports.create = function (newUser, callback){
    "use strict";
    bcrypt.genSalt(10, function(err,salt){
        bcrypt.hash(newUser.password, salt, function(err,hash){
            if (err) {
                console.log(err);
            }
            newUser.password = hash;
            newUser.save(callback);
        });
    });
};
module.exports.getById = function(id, callback){
    "use strict";
    var query = { _id: id};
    Membre.findOne(query, callback);
};
module.exports.getUserByUsername = function (username, callback){
    "use strict";
    var query = { username: username};
    Membre.findOne(query, callback);
};
module.exports.update = function(id, params, callback){

};
module.exports.comparePassword = function(checkPassword, hash, callback){
    "use strict";
    bcrypt.compare(checkPassword, hash, function (err, isMatch){
        callback(err,isMatch);
    });
};
var mongoose = require('mongoose');


var CounterSchema = mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    seq: {
        type: Number,
        default: 0
    }
});
var Counter =
    module.exports =
    mongoose.model('Counter', CounterSchema);
var isReady = false;

module.exports.instanciate = function(id){
    Counter.findOne({_id: id},function (err, counter) {
        if (!counter) {
            // If no counter exists then create one and save it.
            counter = new Counter({_id: id});
            counter.save();
        }
    });
};
module.exports.getNextVal = function(id, callback){
    Counter.findByIdAndUpdate({_id: id}, {$inc: {seq: 1}}, {new: true}, function (err, counter) {
        callback(err, counter.seq);
    });
};
module.exports.revertVal = function(id){
    Counter.findByIdAndUpdate({_id: id}, {$inc: {seq: -1}}, {new: true});
};
module.exports.getCurrVal = function(id, callback){
    "use strict";
    Counter.findById({_id: id}, function (err, counter) {
        callback(err, counter.seq);
    });
};